// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const { dialog } = require('electron').remote

const fs          = require('fs')
const path        = require('path')

// imgmin plugins
const imagemin                = require('imagemin')
const imageminJpegRecompress  = require('imagemin-jpeg-recompress')
const imageminJpegtran        = require('imagemin-jpegtran')
const imageminPngquant        = require('imagemin-pngquant')
const imageminOptipng         = require('imagemin-optipng')
const imageminGifsicle        = require('imagemin-gifsicle')
const imageminSvgo            = require('imagemin-svgo');
const imageminWebp            = require('imagemin-webp');

// html elements
let folderSelect = document.getElementById('folderSelect')
let selectedFolder = document.getElementById('selectedFolder')
let done = document.getElementById('done')
let submit = document.getElementById('submit')
let option = document.getElementById('option')
let animation = document.getElementById('gif')
let selectedFolderName = ''

// select folder
folderSelect.addEventListener('click', (e) => {
  dialog.showOpenDialog({
    properties: ['openDirectory']
  }).then(result => {
    if (!result.canceled && result.filePaths) {
      const folderName = result.filePaths[0]

      selectedFolder.innerHTML = folderName
      selectedFolderName = folderName

      // show options and mifniy
      submit.classList.remove('hide')
      option.classList.remove('hide')
    }
  }).catch(error => {
    // error
  })
})

// minify click
submit.addEventListener('click', (e) => {
  done.innerHTML = 'Minifying ...'
  animation.classList.remove('hide')
  folderSelect.disabled = true
  submit.disabled = true
  option.disabled = true

  const ext = '.{jpg,png,gif,svg,ico}'
  const ext_webp = '.webp'
  let outputDir = selectedFolderName + '/minified_images/'

  // to solve directory structure
  // imagemin has known issue for preserve directory structure
  // TODO: still have issue for 2nd level 3rd level
  fs.readdirSync(selectedFolderName).forEach(file => {
    let fileName = path.join(selectedFolderName, file)
    let isDir = fs.statSync(fileName).isDirectory()

    if (!isDir) {
      if (path.extname(file).toLowerCase() === ext_webp) {
        minify([selectedFolderName + '/*' + ext_webp], outputDir, true)
      }
      minify([selectedFolderName + '/*' + ext], outputDir, false)
    }

  })
})

// minify imagemin
function minify(inputSource, outputDir, webp) {
  // image quality
  let quality = {
    gif: {
      interlaced: true
    },
    png: {
      optimizationLevel: 3
    },
    pngq: {
      quality: [0.6, 0.8]
    },
    svg: {
      plugins: [{
        removeViewBox: true
      }]
    },
    jpg: {
      quality: 'medium',
      target: 0.9999,
      min: 40,
      max: 95
    },
    webp: {
      quality: 75
    }
  };

  // less optimization
  if (~~option.value === 1) {
    quality.png.optimizationLevel = 3
    quality.pngq.quality = [0.45, 0.6]
    quality.jpg.quality = 'low'
    quality.jpg.target = 0.6
    quality.jpg.min = 45
    quality.jpg.max = 60
    quality.webp.quality = 45
  }

  // medium optimization
  if (~~option.value === 2) {
    quality.png.optimizationLevel = 5
    quality.pngq.quality = [0.6, 0.75]
    quality.jpg.quality = 'medium'
    quality.jpg.target = 0.75
    quality.jpg.min = 60
    quality.jpg.max = 75
    quality.webp.quality = 55
  }

  // higher optimization
  if (~~option.value === 3) {
    quality.png.optimizationLevel = 7
    quality.pngq.quality = [0.75, 0.9]
    quality.jpg.quality = 'high'
    quality.jpg.target = 0.9
    quality.jpg.min = 75
    quality.jpg.max = 90
    quality.webp.quality = 65
  }

  // imagemin
  if (webp) {
    imagemin(inputSource, outputDir, {
      use: [
        imageminWebp(quality.webp)
      ]
    }).then(files => {
      done.innerHTML = 'Done. <br>Your minified images are in - ' + selectedFolderName + '\\minified_images\\'
      folderSelect.disabled = false
      submit.disabled = false
      option.disabled = false
      animation.classList.add('hide')
    }).catch(err => {
      done.innerHTML = err
      folderSelect.disabled = false
      submit.disabled = false
      option.disabled = false
      animation.classList.add('hide')
    })
  } else {
    imagemin(inputSource, {
      destination: outputDir,
      plugins: [
        imageminGifsicle(quality.gif),
        imageminJpegtran(),
        imageminOptipng(quality.png),
        imageminPngquant(quality.pngq),
        imageminSvgo(quality.svg),
        imageminJpegRecompress(quality.jpg)
      ]
    }).then(files => {
      done.innerHTML = 'Done. <br>Your minified images are in - ' + selectedFolderName + '\\minified_images\\'
      folderSelect.disabled = false
      submit.disabled = false
      option.disabled = false
      animation.classList.add('hide')
    }).catch(err => {
      done.innerHTML = err
      folderSelect.disabled = false
      submit.disabled = false
      option.disabled = false
      animation.classList.add('hide')
    })
  }

}
